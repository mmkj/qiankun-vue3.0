/*
 * @Author: lizhijie429
 * @Date: 2021-07-22 20:25:53
 * @LastEditors: lizhijie429
 * @LastEditTime: 2021-07-26 15:23:14
 * @Description:
 */
export enum TabsMutationsType {
  UPDATE_TABS_LIST = "UPDATE_TABS_LIST",
  UPDATE_TABS_HOVER = "UPDATE_TABS_HOVER",
  REMOVE_LAST_TAB = "REMOVE_LAST_TAB",
  REMOVE_ANY_TAB = "REMOVE_ANY_TAB",
}
export enum TabsActionsType {
  UPDATE_TABS_LIST = "UPDATE_TABS_LIST",
  UPDATE_TABS_HOVER = "UPDATE_TABS_HOVER",
  REMOVE_TABS_ITEM = "REMOVE_TABS_ITEM",
}
